+++
title = "YAGoLL (Yet Another Game of Life Library)"
date = 2022-08-28
[taxonomies]
categories = [ "projects" ]
tags = [ "rust", "simulation", "cellular" ]
[extra]
repo = "https://github.com/chrisrenfrow/yagoll"
+++

*A toy project I used to learn how to design and publish a crate.*

<!-- more -->

{{ project_summary() }}

This project was started as a way to get myself (re-)acquainted with Rust. Specifically, I wanted to learn more about structuring and publishing crates. 
