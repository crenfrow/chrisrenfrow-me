+++
title = "Zulipsis"
date = 2023-11-09
[taxonomies]
categories = [ "projects" ]
tags = [ "rust" ]
[extra]
start = 2023-10-10
repo = "https://github.com/ChrisRenfrow/zulipsis"
version = "0.1.0"
language = "Rust"
+++

*A configurable and easy to use CLI program that changes your Zulip status at regular intervals.*

<!-- more -->

{{ project_summary() }}

I started this project on a whim when I thought it would be kind of cute/fun to cycle my Zulip status message to parody loading screen messages (e.g. "reticulating splines..."). I kept adding more features to it and now it's in a pretty stable state, though I may continue to improve it in the future.
