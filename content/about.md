+++
title = "about"
[extra]
disable_toc = true
+++

# Hello
I am a human that writes human-friendly software. I love to learn, and try to do so generously.

# This is where I…
- [publish my thoughts](/blog) for nobody in particular to read
- [work in the open](/blog) so you can learn from my mistakes and occasional triumphs
- [show-off my projects](/projects) in various stages of growth/decay

# Other places to find me:
- [GitHub](https://github.com/ChrisRenfrow)
- [GitLab](https://gitlab.com/crenfrow)
- <a rel="me" href="https://recurse.social/@crenfrow">Mastodon</a>
