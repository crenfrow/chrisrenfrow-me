**Started:** {% if page.extra.start %}{{ page.extra.start }}{% else %}{{ page.date }}{% endif %} \
{% if page.extra.repo -%}
**Repo:** [{{ page.extra.repo | replace(from="https://", to="") }}]({{ page.extra.repo }}) \
{%- endif %}
{% if page.extra.version -%}
**Version:** {{ page.extra.version }} \
{%- endif %}
{% if page.extra.language -%}
**Language:** {{ page.extra.language }} \
{%- endif %}
