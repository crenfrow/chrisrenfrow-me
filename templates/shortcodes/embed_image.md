<img src="{{ path }}" alt="{{ alt }}" title="{{ alt }}" />
{% if caption %}
> {{ caption }}
{% endif %}
